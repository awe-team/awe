---
title: Notification module
author: Pablo García
author_title: Senior Software Engineer @ Almis
author_url: https://gitlab.com/pablo.garcia.almis
author_image_url: https://gitlab.com/uploads/-/system/user/avatar/3234791/avatar.png?width=400
tags: [awe, notificación, subscripción, novedad]
---

<img style={{ width: "60%", margin: "10% 20%" }}
alt="AWE security"
src={require('@docusaurus/useBaseUrl').default('img/undraw_notifications.svg')}
/>

`AWE Framework` now has a new _module_: **Notification module**.

This module allows _very easily_ to create new topics and publish notifications over them.

We also have developed a new `settings` screen to allow the user to subscribe to topics which he or she prefers via **notification** or **email**.

To see more information about this new functionality, you can read about it on [this link](/docs/guides/notifier)
