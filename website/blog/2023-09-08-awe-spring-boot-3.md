---
title: AWE get Spring Boot 3
author: Pablo Vidal
author_title: Senior Software Engineer @ Almis
author_url: https://gitlab.com/limkin
author_image_url: https://gitlab.com/uploads/-/system/user/avatar/3234812/avatar.png?width=400
tags: [awe, spring, spring boot 3, novedad]
---

<img style={{ width: "60%", margin: "10% 20%", padding: "50" }} 
    alt="Spring Boot 3" 
    src={require('@docusaurus/useBaseUrl').default('img/blog/spring-boot-3.png')}
/>

The AWE framework team is pleased to announce that starting with version 4.7.1, 
AWE will use the Spring Boot 3 and Spring 6 version as a base image. 
In this way, we will be prepared for new functionalities, greater security and of course, better maintenance.

This version requires us to use **Java 17**, so you have to take this into account if you want to use the new version of AWE.

To see more information about the new features of Spring Boot 3, you can see it on [this link](https://www.baeldung.com/spring-boot-3-spring-6-new).

Next, we leave you the [migration guide](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-3.0-Migration-Guide) for AWE applications that use the new version of Spring Boot 3.
