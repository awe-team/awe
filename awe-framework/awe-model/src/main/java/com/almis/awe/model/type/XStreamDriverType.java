package com.almis.awe.model.type;

public enum XStreamDriverType {
  DOM,
  STAX,
  XPP
}
