package com.almis.awe.model.component;

import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.Data;

@Data
public class RequestDataHolder {
  private ObjectNode requestData;
}
