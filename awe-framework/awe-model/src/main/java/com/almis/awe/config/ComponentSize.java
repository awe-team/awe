package com.almis.awe.config;

/**
 * Component sizes enumerated
 */
public enum ComponentSize {
  /**
   * Small size
   */
  SM,
  /**
   * Medium size
   */
  MD,
  /**
   * Large size
   */
  LG
}
