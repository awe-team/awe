package com.almis.awe.testing.enumerated;

public enum MenuBehavior {
  CLICK_ALL,
  CLICK_FIRST_AND_OPTION,
  CLICK_OPTION
}
