package com.almis.awe.testing.enumerated;

public enum RowEditBehavior {
  SINGLE_CLICK,
  DOUBLE_CLICK
}
