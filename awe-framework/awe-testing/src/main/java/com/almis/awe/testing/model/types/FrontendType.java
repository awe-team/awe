package com.almis.awe.testing.model.types;

public enum FrontendType {
  /**
   * React JS frontend enum value
   */
  REACT,
  /**
   * Angular JS frontend enum value
   */
  ANGULAR
}
