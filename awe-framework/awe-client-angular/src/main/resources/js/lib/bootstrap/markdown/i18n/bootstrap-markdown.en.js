/**
 * Spanish translation for bootstrap-markdown
 * by Leandro Poblet <leandrodrhouse@gmail.com>
 */
(function($){
  $.fn.markdown.messages['en-GB'] = {
    'Bold': "Bold",
    'Italic': "Italic",
    'Heading': "Heading",
    'URL/Link': "URL/Link",
    'Image': "Image",
    'List': "List",
    'Preview': "Preview",
    'strong text': "strong text",
    'emphasized text': "emphasized text",
    'heading text': "heading text",
    'enter link description here': "enter link description here",
    'Insert Hyperlink': "Insert Hyperlink",
    'enter image description here': "enter image description here",
    'Insert Image Hyperlink': "Insert Image Hyperlink",
    'enter image title here': "enter image title here",
    'list text here': "list text here"
  };
});
