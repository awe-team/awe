package com.almis.awe.developer.model;

public interface ITranslationResult {
  String getTranslation();
  String getRemaining();
}
