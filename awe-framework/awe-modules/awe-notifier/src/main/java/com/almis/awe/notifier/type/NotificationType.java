package com.almis.awe.notifier.type;

public enum NotificationType {
  NORMAL,
  OK,
  INFO,
  WARNING,
  ERROR
}
