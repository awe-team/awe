package com.almis.awe.annotation.autoconfigure;

import com.almis.awe.annotation.aspect.*;
import com.almis.awe.annotation.processor.locale.LocaleProcessor;
import com.almis.awe.annotation.processor.security.CryptoProcessor;
import com.almis.awe.annotation.processor.security.HashProcessor;
import com.almis.awe.annotation.processor.session.SessionProcessor;
import com.almis.awe.config.BaseConfigProperties;
import com.almis.awe.model.component.AweElements;
import com.almis.awe.model.component.AweSession;
import com.almis.awe.service.EncodeService;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Class load annotation processor definitions
 *
 * Also enables Aspect definitions with Auto-proxying
 *
 * @author dfuentes
 * Created by dfuentes on 26/05/2017.
 */
@AutoConfiguration
@EnableConfigurationProperties(value = BaseConfigProperties.class)
@EnableAspectJAutoProxy
public class AnnotationConfig {

  /////////////////////////////////////////////
  // PROCESSORS
  /////////////////////////////////////////////

  /**
   * Locale processor
   * @param aweSessionObjectFactory AWE session object factory
   * @param aweElementsObjectFactory AWE element object factory
   * @return Locale processor bean
   */
  @Bean
  @ConditionalOnMissingBean
  public LocaleProcessor localeProcessor(final ObjectFactory<AweSession> aweSessionObjectFactory,
                                         final ObjectFactory<AweElements> aweElementsObjectFactory) {
    return new LocaleProcessor(aweSessionObjectFactory, aweElementsObjectFactory);
  }

  /**
   * Session processor
   * @param aweSessionObjectFactory AWE session object factory
   * @return Session processor bean
   */
  @Bean
  @ConditionalOnMissingBean
  public SessionProcessor sessionProcessor(final ObjectFactory<AweSession> aweSessionObjectFactory) {
    return new SessionProcessor(aweSessionObjectFactory);
  }

  /////////////////////////////////////////////
  // ANNOTATIONS
  /////////////////////////////////////////////

  /**
   * Audit annotation
   * @return Audit annotation bean
   */
  @Bean
  @ConditionalOnMissingBean
  public AuditAnnotation auditAnnotation() {
    return new AuditAnnotation();
  }

  /**
   * Crypto annotation
   * @return Crypto annotation bean
   */
  @Bean
  @ConditionalOnMissingBean
  public CryptoAnnotation cryptoAnnotation(CryptoProcessor cryptoProcessor) {
    return new CryptoAnnotation(cryptoProcessor);
  }

  /**
   * CryptoProcessor bean
   * @param encodeService Encode service
   * @return CryptoProcessor bean
   */
  @Bean
  @ConditionalOnMissingBean
  public CryptoProcessor cryptoProcessor(EncodeService encodeService) {
    return new CryptoProcessor(encodeService);
  }

  /**
   * Download Annotation
   * @param baseConfigProperties Base config properties
   * @return Download Annotation bean
   */
  @Bean
  @ConditionalOnMissingBean
  public DownloadAnnotation downloadAnnotation(BaseConfigProperties baseConfigProperties) {
    return new DownloadAnnotation(baseConfigProperties);
  }

  /**
   * GoTo Annotation
   * @return GoTo Annotation bean
   */
  @Bean
  @ConditionalOnMissingBean
  public GoToAnnotation goToAnnotation() {
    return new GoToAnnotation();
  }

  /**
   * Hash Annotation
   * @return Hash Annotation bean
   */
  @Bean
  @ConditionalOnMissingBean
  public HashAnnotation hashAnnotation(HashProcessor hashProcessor) {
    return new HashAnnotation(hashProcessor);
  }

  /**
   * HashProcessor bean
   * @return HashProcessor bean
   */
  @Bean
  @ConditionalOnMissingBean
  public HashProcessor hashProcessor(EncodeService encodeService) {
    return new HashProcessor(encodeService);
  }

  /**
   * Locale Annotation
   * @param localeProcessor Locale processor
   * @return Locale Annotation bean
   */
  @Bean
  @ConditionalOnMissingBean
  public LocaleAnnotation localeAnnotation(LocaleProcessor localeProcessor) {
    return new LocaleAnnotation(localeProcessor);
  }

  /**
   * Session Annotation
   * @param sessionProcessor Session processor
   * @return Session annotation bean
   */
  @Bean
  @ConditionalOnMissingBean
  public SessionAnnotation sessionAnnotation(SessionProcessor sessionProcessor) {
    return new SessionAnnotation(sessionProcessor);
  }
}

